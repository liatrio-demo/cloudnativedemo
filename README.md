# Infrastructure Terraform Configuration

## Prerequisites 

Most of the configuration is automated through the terraform apply except for the following:

1. aws route 53 dns zone and records are created manually
2. temporary letsencrypt ssl certificates are generated using the following script, these last for 90 days
3. Ambassador (Emissary-Ingress) requires a self-signed cert, see the Improvements section at the end of this document

```bash

# Set the <URL>, <EMAIL>

docker run -it --rm --name certbot \
--env AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
--env AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
-v "$PWD:/etc/letsencrypt" \
-v "$PWD:/var/lib/letsencrypt" \
certbot/dns-route53 certonly \
-d <URL> \
-m <EMAIL> \
--dns-route53 \
--agree-tos \
--non-interactive


aws acm import-certificate \
--certificate "fileb://$PWD/live/<URL>cert.pem" \
--private-key "fileb://$PWD/live/<URL>/privkey.pem" \
--certificate-chain "fileb://$PWD/live/<URL>/fullchain.pem"

```

3. Set the following variables in the gitlab CI Enviroment using the gitlab UI (Project->CI/CD Settings/Variables) :
    - AWS_ACCESS_KEY_ID
    - AWS_SECRET_ACCESS_KEY
    - AWS_REGION - the aws rtegion for deployment
    - TF_VAR_AGENT_TOKEN - see Gitlab Agent configuration below

The AWS access keys should be generated for a iam user that has permissions to create the required VPC/EKS configurations and related items. K8s cluster release are managed by helm using the k8s cluster certificate for authentication.

## Instructions

1. Generate an agent token in the gitlab UI, [Agent Configuration Document](https://docs.gitlab.com/ee/user/clusters/agent/install/)
2. Apply the terraform configuration using the gitlab CI
3. Setup local admin access to the cluster with the following command:
    - `aws eks update-kubeconfig --name LiatrioDemo`


### Gitlab CI

Defined in `./gitlab-ci.yaml` it contains the following stages:

1. Validate
    - fmt - checks the code conforms to terraforms built in linter
    - validate - checks the code sytax
2. Build
    - runs the terraform plan stage, this will display a change document in the CI log
3. Deploy
    - Currently set as a manual step runnable on the `main` branch
    - applies the terraform configuration
4. Cleanup
    - Performas a terraform destroy

## Gitlab Agent Configuration

The configuration file is stored here:
- .gitlab/agents/primary-agent/config.yaml

This file defines the paths that are monitored by the agent for k8s manifests to be applied to the cluster. The agent release is managed as a helm release in the `infrastructure/` directory

This directory holds configfuration for the following:

- vpc - using a custom module stored locally
- eks cluster - implemented using a publicly available terraform module
- helm configuration - releases for the Gitlab Agent and Ambassador API Gateway (Emissary-Ingress)

## Repository Structure

In `infrastructure/`
- vpc/ - contains the vpc module
- manifests/ - contains values file for the api gateway
- infrastructure-releases.tf - contains the current helm_release configurations
- ingress.tf - contains the external load balancer configuration
- main.tf - contains the vpc/eks module configurations, dns entrieds
- provider.tf - contains the terraform provider configuration
- securitygroups.tf - contains the ALB and EKS node security group definitions

In `manifest/`:

This configuration is automatically applied to cluster on a pull basis by the gitlab-agent.

- ambassador/ - contains configuration for ambassador/emissary-ingress
- argocd/ - contains configuration for argocd 
- eks-metrics/ - contains configuration for the eks metrics server, used by prometheus
- monitoring/ - contains configuration for grafana/prometheus

- ambassador-configuration.yaml - contains the gateway routing/listener and host configuration

### Improvements

- set up centralized logging using ELK/Splunk
- move helm configurstion and manifest to another repository to seprate infrastructure applications and the infrastructure platform
- SSL certificate creation/rotation and dns setup can be automated through terraform
- A proper VPN should be used to access/debug cluster nodes, current solution can be managed by manually launching a bastion node with a public IP in the DMZ and using security groups to control access
- when multiple enviroments are required, releases can be managed for all envs (CI/UAT/Prod) using release branches and/or terraform workspaces, if the configuration varies between envs then separate repositories would be required
- external EKS endpoint is required to communicate with gitlab and for local use due to the lack of a proper VPN
- Emissary-Ingress requires a self signed cert to run and accept secure connections, the enterprise version automates the process which was initially done maunally:

```bash
kubectl create secret -n emissary tls tls-cert --cert=cert.pem --key=key.pem
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -subj '/CN=ambassador-cert' -nodes
```
