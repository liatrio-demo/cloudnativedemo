# secuirtygroups.tf

resource "aws_security_group" "ambassador" {
  name        = "${local.cluster_name}-SG-ALB-${local.alb_project}"
  description = "External ALB Security Group"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "http ingress rule"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "https ingress rule"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(
    local.shared_tags,
    {
      Name = "${local.cluster_name}-SG-ALB-${local.alb_project}"
      Type = "Security Group"
    },
  )

  lifecycle {
    create_before_destroy = true
  }
}

# https://anywhere.eks.amazonaws.com/docs/reference/ports/
resource "aws_security_group" "eks_node" {
  name        = "${local.cluster_name}-SG-EKS-${local.alb_project}"
  description = "External ALB Security Group"
  vpc_id      = module.vpc.vpc_id
  ingress {
    description     = "https ingress rule"
    from_port       = 32443
    to_port         = 32443
    protocol        = "tcp"
    cidr_blocks     = [module.vpc.cidr_block]
    security_groups = [aws_security_group.ambassador.id]
  }
  ingress {
    description = "worker node intercommunication"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self        = true
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(
    local.shared_tags,
    {
      Name = "${local.cluster_name}-SG-EKS-${local.alb_project}"
      Type = "Security Group"
    },
  )

  lifecycle {
    create_before_destroy = true
  }
}