# key.tf 

resource "aws_key_pair" "default" {
  key_name   = "${local.project_name} Default Key"
  public_key = file(var.public_key_path)
}