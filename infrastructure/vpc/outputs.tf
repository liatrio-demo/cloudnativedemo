# outputs.tf

output "keypair_name" {
  value       = aws_key_pair.default.key_name
  description = "Default key pair for the VPC"
}

output "vpc_dmz_subnets" {
  value       = aws_subnet.dmz.*.id
  description = "DMZ subnets list"
}

output "vpc_app_subnets" {
  value       = aws_subnet.app.*.id
  description = "App subnets list"
}

output "vpc_id" {
  value       = aws_vpc.default.id
  description = "The created VPC id"
}

output "cidr_block" {
  value       = var.cidr_block
  description = "VPC CIDR Block"
}
