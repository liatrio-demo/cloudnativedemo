# Terraform Exercise

## Prerequisites

- built and tested with terraform version 1.2.3 
- ensure the environment variable AWS_PROFILE contains a valid profile

## Instructions

```bash
$ terraform apply

Apply complete! Resources: 36 added, 0 changed, 0 destroyed.

Outputs:

alb_cname = "Example-ALB-36523818.us-west-2.elb.amazonaws.com"

$ curl Example-ALB-36523818.us-west-2.elb.amazonaws.com:80
<h1>Hello, Veem!</h1>
```

## File Structure

- gateway.tf - contains the internet and nat gateway resources
- locals.tf - main configuration parameters are here
- outputs.tf - external alb cname is ouout here
- provider.tf - aws configuration, default to valid environment $AWS_PROFILE
- routetables.tf - all route table configurations for public and private subnets
- subnets.tf - create DMZ (public) and app (private) subnets
- (terraform.tf) - can be implemented for alternate backends (defaults to local)
- versions.tf - version lock file
- vpc.tf - creates the vpc
- keys/ - contains public/private key for ec2 access

# Improvements

- add logic to provision an arbitray number of vpcs
- test with more vpc configurations and added subnets, add logic to confirm the requested vpc configuration will work before proceeding with the deployment
- include randomized hex codes in the naming convention to avoid naming collisions in larger accounts
- include vpc flow logs for activity tracking
- implement backend for shared state (s3, artifactory, terraform enterprise)