# subnets.tf

data "aws_availability_zones" "available" {
  state = "available"

  filter {
    name   = "group-name"
    values = ["us-west-2"]
  }

}

# Public Subnet
resource "aws_subnet" "dmz" {
  count                   = local.az_total
  vpc_id                  = aws_vpc.default.id
  cidr_block              = element(local.public_subnet_cidrs, count.index) # assign the first set of available cidrs 
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  map_public_ip_on_launch = true

  tags = merge(
    local.shared_tags,
    {
      Name        = format("${local.project_name}-DMZ-%03d", count.index + 1)
      Description = "${format("DMZ Subnet #%03d", count.index + 1)}"
      Type        = "Subnet"
      Tier        = "${element(local.public_subnet_cidrs, count.index)}:DMZ"
    },
  )
}

# Private Subnet
resource "aws_subnet" "app" {
  count                   = local.az_total
  vpc_id                  = aws_vpc.default.id
  cidr_block              = element(local.public_subnet_cidrs, count.index + local.az_total) #assign the second set of available cidrs
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  map_public_ip_on_launch = false

  tags = merge(
    local.shared_tags,
    {
      Name        = format("${local.project_name}-APP-%03d", count.index + 1)
      Description = "${format("APP Subnet #%03d", count.index + 1)}"
      Type        = "Subnet"
      Tier        = "${element(local.public_subnet_cidrs, count.index + local.az_total)}:APP"
    },
  )
}

# TODO: Implement additional subnets accepting a list of subnet names