# variables.tf

# cluster configuration
variable "cidr_block" {
  type        = string
  default     = "10.0.0.0/24" # 255 addresses
  description = "Cidr block reserved for the vpc"
}

# This feature is not fully implemented
variable "subnet_total" {
  type        = number
  default     = 2 # Currently this module only supports 2 subnets (DMZ and APP)
  description = "The total number of subnets contained in this vpc"
}

variable "az_total" {
  type        = number
  default     = "3"
  description = "The number of availability zones included in this vpc"
}

variable "environment" {
  type        = string
  description = "Deployment environment tag applied to created resources, ie Test/CI/QA/UAT/Production"
}

variable "source_repo" {
  type        = string
  description = "Location of the source code implementing this module"
}

variable "project_name" {
  type        = string
  description = "Project tag applied to created resources"
}

variable "public_key_path" {
  type        = string
  description = "Path to the pregenerated public key used as the default key pair for this vpc"
}
