# locals.tf

data "aws_region" "current" {}

locals {
  project_name    = var.project_name
  cidr_block      = var.cidr_block
  subnet_total    = var.subnet_total
  az_total        = var.az_total
  public_key_path = var.public_key_path

  # public_subnet_cidrs contains a list of cidrs for each subnet in the vpc. 
  #   This is calculated by:

  #     netnumber - total number of subnets in the vpc (availability zones * subnet sets (dmz/app is 2))
  #     subnet_bits - number of bits added to the cidr block mask for subnet calculation
  #     cidrsubnet - function that returns the subnet cidrs, extra cidrs will be preserved but left unused
  subnet_bits         = ceil(log(local.subnet_total * local.az_total, 2))
  public_subnet_cidrs = [for netnumber in range(0, local.subnet_total * local.az_total) : cidrsubnet(local.cidr_block, local.subnet_bits, netnumber)] # See: https://github.com/cloudposse/terraform-aws-dynamic-subnets/blob/master/docs/design.md

  shared_tags = {
    "Environment"      = var.environment              # Test/CI/QA/UAT/Production
    "AvailabilityZone" = data.aws_region.current.name # availability zone
    "Manager"          = "Terraform"                  # terraform deployed resource
    "Source"           = var.source_repo              # location of source code
    "Project"          = local.project_name           # project name
  }
}
