# gateway.tf

# Internet Gateway

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.default.id

  tags = merge(
    local.shared_tags,
    {
      Name = "${local.project_name}-IGW"
    },
  )
}

# Nat Gateway

resource "aws_eip" "gateway_eip" {
  vpc = true

  tags = merge(
    local.shared_tags,
    {
      Name        = "${local.project_name}-EIP-001"
      Description = "NAT Gateway EIP"
      Type        = "Elastic IP"
    },
  )
}

resource "aws_nat_gateway" "ng" {

  allocation_id = aws_eip.gateway_eip.id
  subnet_id     = element(aws_subnet.dmz.*.id, 0) # public subnet

  tags = merge(
    local.shared_tags,
    {
      Name        = "${local.project_name}-NAT-001"
      Description = "Default Egress"
      Type        = "NAT Gateway"
    },
  )
}