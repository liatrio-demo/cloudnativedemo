# vpc.tf

resource "aws_vpc" "default" {
  cidr_block       = local.cidr_block
  instance_tenancy = "default"

  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    local.shared_tags,
    {
      Name        = "${local.project_name} VPC"
      Description = "${local.project_name} VPC"
      Type        = "VPC"
    }
  )
}

