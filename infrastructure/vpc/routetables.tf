# routetables.tf

data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [aws_vpc.default.id]
  }
}

resource "aws_route_table" "dmz" {
  # count  = length(aws_subnet.dmz)
  vpc_id = aws_vpc.default.id

  tags = merge(
    local.shared_tags,
    {
      Name        = "DMZ Route Table"
      Description = "Route table for DMZ"
      Type        = "Route Table"
      Tier        = "N/A"
    },
  )
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.default.id

  tags = merge(
    local.shared_tags,
    {
      Name        = "Private Subnet Route Table"
      Description = "Route table for private subnets in each availability zone"
      Type        = "Route Table"
      Tier        = "N/A"
    },
  )
}

# Associate DMZ subnets with public route table
resource "aws_route_table_association" "dmz" {
  count = length(aws_subnet.dmz)
  depends_on = [
    aws_route_table.dmz
  ]
  subnet_id      = element(aws_subnet.dmz.*.id, count.index)
  route_table_id = aws_route_table.dmz.id
}

# Associate app subnets for private route table
resource "aws_route_table_association" "app" {
  count = length(aws_subnet.app)
  depends_on = [
    aws_route_table.private,
  ]
  subnet_id      = element(aws_subnet.app.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}

# Route for Internet Gateway
resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_route_table.dmz.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw.id
}

# Routes for NAT Gateway, one for each private subnet
resource "aws_route" "private_nat_gateway" {
  route_table_id         = aws_route_table.private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.ng.id
}