# backend.tf 

# use the gitlab terraform backend
terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/37259632/terraform/state/0"
    lock_address   = "https://gitlab.com/api/v4/projects/37259632/terraform/state/0/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/37259632/terraform/state/0/lock"
  }
}
