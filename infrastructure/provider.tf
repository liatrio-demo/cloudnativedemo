# provider.tf

# Default Region
provider "aws" {
  region = "us-west-2"
}

data "aws_eks_cluster_auth" "k8s" {
  name = module.eks.cluster_id
}

provider "helm" {
  kubernetes {
    host                   = module.eks.cluster_endpoint
    cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
    token                  = data.aws_eks_cluster_auth.k8s.token
  }
}
