# ingress

# Internet ALB

locals {
  alb_project = "Ambassador"
  blue_asg    = element(module.eks.eks_managed_node_groups_autoscaling_group_names, 0)
  green_asg   = element(module.eks.eks_managed_node_groups_autoscaling_group_names, 1)
  ssl_policy  = "ELBSecurityPolicy-FS-1-2-Res-2020-10"
}

# Certificates, provisioned using lets encrypt
data "aws_acm_certificate" "liatrio" {
  domain   = "liatrio.ulisseas.com"
  statuses = ["ISSUED"]
}

# Main ALB, targeting the Ambassador Gateway for ingress

resource "aws_lb" "external" {
  name               = "${local.cluster_name}-ALB-Ambassador"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.ambassador.id]
  subnets            = module.vpc.vpc_dmz_subnets

  enable_deletion_protection = true
  tags = merge(
    local.shared_tags,
    {
      Type = "Application Load Balancer"
    },
  )
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.external.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      host        = "#{host}"
      path        = "/#{path}"
      query       = "#{query}"
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "https" {
  load_balancer_arn = aws_lb.external.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = local.ssl_policy
  certificate_arn   = data.aws_acm_certificate.liatrio.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.https.arn
  }
}

resource "aws_lb_target_group" "https" {
  name     = "${local.alb_project}-ATG-https"
  port     = 32443
  protocol = "HTTPS"
  vpc_id   = module.vpc.vpc_id

  health_check {
    protocol = "HTTPS"
    path     = "/health"
    port     = 32443
    interval = 5
  }

  tags = merge(
    local.shared_tags,
    {
      Description = "HTTPS Target Group for ${local.cluster_name}-ALB-${local.alb_project} Load Balancer"
      Type        = "Target Group"
    },
  )
}

resource "aws_autoscaling_attachment" "ambassador" {
  autoscaling_group_name = local.green_asg
  alb_target_group_arn   = aws_lb_target_group.https.arn
}
