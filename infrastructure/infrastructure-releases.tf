# infrastructure-releases.tf

# Gitlab Agent configuration

# this variable is set as an environment variable in gitlab, it will not show in terraform output
variable "AGENT_TOKEN" {
  type     = string
  nullable = false
  # sensitive = true
}

resource "helm_release" "gitlab_primary_agent" {
  name = "primary-agent"

  repository = "https://charts.gitlab.io"
  chart      = "gitlab-agent"
  version    = "v1.3.0"

  namespace        = "gitlab-agent"
  create_namespace = true

  set {
    name  = "image.tag"
    value = "v15.2.0"
  }

  set_sensitive {
    name  = "config.token"
    value = var.AGENT_TOKEN
  }

  set {
    name  = "config.kasAddress"
    value = "wss://kas.gitlab.com"
  }
}

# Ambassador opensource project has been renamed to Emissary-Ingress
resource "helm_release" "ambassador" {
  name = "emissary-ingress"

  repository = "https://app.getambassador.io"
  chart      = "emissary-ingress"
  version    = "v8.1.0"

  namespace        = "emissary"
  create_namespace = true

  values = [file("./manifests/ambassador-values.yaml")]
}

resource "helm_release" "prometheus" {
  name = "prometheus"

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus"
  version    = "v15.12.0"

  namespace        = "monitoring"
  create_namespace = true

  values = [file("./manifests/prometheus-values.yaml")]
}

resource "helm_release" "grafana" {
  name = "grafana"

  repository = "https://grafana.github.io/helm-charts"
  chart      = "grafana"
  version    = "v6.32.11"

  namespace        = "monitoring"
  create_namespace = true

  # setup auto importing of configuration
  set {
    name  = "sidecar.dashboards.enabled"
    value = "true"
  }

  set {
    name  = "sidecar.dashboards.labelValue"
    value = "1"
  }

  set {
    name  = "sidecar.datasources.enabled"
    value = "true"
  }
}

resource "helm_release" "argocd" {
  name = "argocd"

  repository = "https://argoproj.github.io/argo-helm"
  chart      = "argo-cd"
  version    = "v4.10.5"

  namespace        = "argocd"
  create_namespace = true
}
