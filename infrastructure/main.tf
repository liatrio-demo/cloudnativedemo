# main.tf

# The main vpc

locals {
  cluster_name = "LiatrioDemo"
  project_name = "Liatrio Demo"
  shared_tags = {
    Environment      = "Demo"
    Project          = local.project_name
    AvailabilityZone = "us-west-2"
    Source           = "https://gitlab.com/liatrio-demo/cloudnativedemo"
    Manager          = "Terraform"
  }
}

module "vpc" {
  source = "./vpc"

  cidr_block = "10.0.0.0/20"
  az_total   = 2 # this module evenly divides the cidr block among # of subnets (az_total * subnet total)

  environment     = "Demo"
  project_name    = local.project_name
  source_repo     = "https://gitlab.com/liatrio-demo/cloudnativedemo"
  public_key_path = "./keys/key.pub"
}

resource "aws_kms_key" "eks" {
  description = "EKS KMS Key"
  depends_on = [
    module.vpc
  ]
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 18.0"

  cluster_name    = local.cluster_name
  cluster_version = "1.22"

  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true

  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
    }
    kube-proxy = {}
    vpc-cni = {
      resolve_conflicts = "OVERWRITE"
    }
  }

  cluster_encryption_config = [{
    provider_key_arn = aws_kms_key.eks.arn
    resources        = ["secrets"]
  }]

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.vpc_app_subnets

  # EKS Managed Node Group(s)
  eks_managed_node_group_defaults = {
    instance_types = ["t2.small"]
  }

  eks_managed_node_groups = {
    blue = {
      min_size     = 0
      max_size     = 1
      desired_size = 0

      instance_types         = ["t2.xlarge"]
      key_name               = module.vpc.keypair_name
      vpc_security_group_ids = [aws_security_group.eks_node.id]
    }
    green = {
      min_size     = 0
      max_size     = 3
      desired_size = 1

      instance_types         = ["t2.xlarge"]
      key_name               = module.vpc.keypair_name
      vpc_security_group_ids = [aws_security_group.eks_node.id]
      #   capacity_type  = "SPOT"
    }
  }

  # aws-auth configmap
  manage_aws_auth_configmap = false

  aws_auth_accounts = [
    "952239833446"
  ]

  tags = merge(
    local.shared_tags,
    {
      Type = "EKS Cluster"
    },
  )

  # https://github.com/terraform-aws-modules/terraform-aws-eks/issues/1753, issue with depends_on and for_each used when creating resources
  #   depends_on = [
  #     module.vpc
  #   ]
}